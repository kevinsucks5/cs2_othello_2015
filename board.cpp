#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}

// helper function keeping track of score of current board
// every position on board has same weight
int Board::score(Side side)
{
    // determining opponent's color
    Side opponent;
    if (side == BLACK)
    {
        opponent = WHITE;
    }
    else
    {
        opponent = BLACK;
    }

    int countAI = 0;
    int countop = 0;
    // counting number of edge pieces for our AI
    for (int i = 1; i < 7; i++)
    {
        if (get(side, 0, i))
        {
            countAI++;
        }
        if (get(side, i, 0))
        {
            countAI++;
        }
    }
    for (int i = 1; i < 7; i++)
    {
        if (get(side, 7, i))
        {
            countAI++;
        }
        if (get(side, i, 7))
        {
            countAI++;
        }
    }

    // counting number of edge pieces for opponent
    for (int i = 1; i < 7; i++)
    {
        if (get(opponent, 0, i))
        {
            countop++;
        }
        if (get(opponent, i, 0))
        {
            countop++;
        }
    }
    for (int i = 1; i < 7; i++)
    {
        if (get(opponent, 7, i))
        {
            countop++;
        }
        if (get(opponent, i, 7))
        {
            countop++;
        }
    }

    // counting number of diagonal adjacent pieces to the corners
    int diag = 0;
    if (get(side, 1, 1))
    {
        diag++;
    }
    if (get(side, 6, 1))
    {
        diag++;
    }
    if (get(side, 1, 6))
    {
        diag++;
    }
    if (get(side, 6, 6))
    {
        diag++;
    }

    // counting number of corner pieces of opponents
    int cornerop = 0;
    if (get(opponent, 0, 0))
    {
        cornerop++;
    }
    if (get(opponent, 0, 7))
    {
        cornerop++;
    }
    if (get(opponent, 7, 0))
    {
        cornerop++;
    }
    if (get(opponent, 7, 7))
    {
        cornerop++;
    }

    // counting number of corner pieces of AI
    int cornerAI = 0;
    if (get(side, 0, 0))
    {
        cornerAI++;
    }
    if (get(side, 0, 7))
    {
        cornerAI++;
    }
    if (get(side, 7, 0))
    {
        cornerAI++;
    }
    if (get(side, 7, 7))
    {
        cornerAI++;
    }

    int nearedge = 0;

    // counting number of adjacent edge pieces for our AI
    for (int i = 1; i < 7; i++)
    {
        if (get(side, 1, i))
        {
            nearedge++;
        }
        if (get(side, i, 1))
        {
            nearedge++;
        }
    }
    for (int i = 1; i < 7; i++)
    {
        if (get(side, 6, i))
        {
            nearedge++;
        }
        if (get(side, i, 6))
        {
            nearedge++;
        }
    }

    if (side == BLACK)
    {
        return (countBlack() - countWhite() + (3 * countAI) - (3 * countop) - (10 * diag)\
         + (7 * cornerAI) - (7 * cornerop) - (5 * nearedge));
    }
    else
    {
        return (countWhite() - countBlack() + (3 * countAI) - (3 * countop) - (10 * diag)\
         + (7 * cornerAI) - (7 * cornerop) - (5 * nearedge));
    }

}