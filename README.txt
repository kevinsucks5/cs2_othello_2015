Stephanie Moon:

- produced the frame work for the simple algorithm
- formulated the overall structure of doMove member function in player
- contributed to the construction of 2-ply depth implementation
- ate oreos

Kevin Chen:

- optimized the heuristic function (score member function in board class)
- constructed the nested for loops for 2-ply depth implementation
- took naps



Improvements:

Initially, our heuristic function simply takes the counts the number of our AI's stones and the opponent's. To better performance, we decided to take special positions into account and weight them differently to produce bias toward advantageous moves. For instances, corner pieces have weights "10", thus highly valued. In contrast, we put negative weights on diagonally adjacent pieces to the corners as well as adjacent edge pieces. As a result, our AI is less likely to put stones in these positions, offering the opponent better positions. Lastly, we added weights on edge pieces (not as high as corners) since they are sufficiently significant to winning Othello. 