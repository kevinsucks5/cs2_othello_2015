#include "player.h"
#include <vector>
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    board = Board();

     if (side == BLACK)
     {
     	color = BLACK;
        oppo_color = WHITE;
     }
     else
     {
     	color = WHITE;
        oppo_color = BLACK;
     }


}

/*
 * Destructor for the player.
 */
Player::~Player() {
}


/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 

    if (opponentsMove != NULL)
    {
        board.doMove(opponentsMove, oppo_color);
    }

    // we can make a move
    if (board.hasMoves(color) == true)
    {
    	Move *opt_move = new Move(-1, -1);
    	
    	if (!testingMinimax) // this is simple heuristic algorithm
    	{
	    	int opt_score = -1000;
	    	for (int i = 0; i < 8; i++)
	    	{
	    		for (int j = 0; j < 8; j++)
	    		{
	    			Move move(i, j);
	    			if (board.checkMove(&move, color)) // valid move
	    			{
	                    Board *temp_board = board.copy();
	                    temp_board->doMove(&move, color);
	    				if (opt_score < temp_board->score(color)) // this board is better
	    				{
	    					opt_score = temp_board->score(color); // update score
	    					opt_move->setX(move.getX());
	                        opt_move->setY(move.getY());
	    				}
	    			}
	    		}
	    	}
	    }
        
		if (testingMinimax) // this is minimax algorithm
		{
			vector<int> worstscores;
			vector<Move> possible_moves;
			
	    	for (int i = 0; i < 8; i++)
	    	{
	    		for (int j = 0; j < 8; j++)
	    		{
	    			Move move(i, j);

	    			if (board.checkMove(&move, color)) // valid move
	    			{
	                    Board *temp_board = board.copy();
	                    temp_board->doMove(&move, color);
	                    int ref = 1000;
	                    for (int u = 0; u < 8; u++)
	                    {
	                    	for (int v = 0; v < 8; v++)
	                    	{
	                    		Move move1(u, v);
	                    		if (temp_board->checkMove(&move1, oppo_color))
	                    		{
	                    			Board *temptemp_board = temp_board->copy();
	                    			temptemp_board->doMove(&move1, oppo_color);
	                    			if (temptemp_board->score(oppo_color) < ref)
	                    			{
	                    				ref = temptemp_board->score(oppo_color);
	                    			}
	                    		}
	                    	}
	                    }

	                    worstscores.push_back(ref);
	                    possible_moves.push_back(move);
	    			}
	    		}
	    	}

	    	int opt_score = worstscores[0];
	    	unsigned int index = 0;
	    	for (unsigned int i = 1; i < worstscores.size(); i++)
	    	{
	    		if (worstscores[i] > opt_score)
	    		{
	    			opt_score = worstscores[i];
	    			index = i;
	    		}
	    	}

	    	opt_move->setX(possible_moves[index].getX());
	        opt_move->setY(possible_moves[index].getY());
		}

    	board.doMove(opt_move, color);
        return opt_move;
    }

    // no possible moves
    else
    {
    	return NULL;
    }
    
}
